# CALPKT Networking Framework

The CALPKT Networking Framework aims to provide you with a very easy implementation of `TCP` or `UDP` networking in Java and Kotlin.    
This framework is event based so there is no need for you to worry about timings etc.       
> It facilitates networking on a low level to avoid bad habbits that are evolving today: Almost every tutorial you see about networking in any language always uses Strings. These strings cause a lot of unnecessary overhead which we want to avoid.

## How it works

```kotlin
val hostname: String = "localhost"
val port = 1535

/*
 * creating a TCP server:
 */
val server = ServerFactory.TCP(hostname, port)

/*
 * creating a UDP server:
 */
val server = ServerFactory.UDP(hostname, port)

```

## Protocols
Both, `TCP` as well as `UDP` support a class called `Protocol` in their constructor. This `Protocol` can bee seen as a given structure on top of our `TCP/IP` stack. With it you can define how the data must look like when the server received something from a remote source.
> Due to the nature of UDP, you can add a `Protocol`, but it may be useless.
```kotlin
val protocol = Protocol(
    ProtocolField("messageID", ProtocolFieldType.INT.length),
    ProtocolField("datetime", 16), // "2019-01-01 00:00".length == 16,
    ProtocolField("temperature", ProtocolFieldType.FLOAT.length)
)

// Server creation:
val server = ServerFactory.TCP(hostname, port, protocol)
val server = ServerFactory.UDP(hostname, port, protocol)
```
This is a sample protocol. The Frameworks combines all Protocolfields to calculate the total length of this protocol.
> Plase Note: Always create your protocol in a way that fits all of your message types, als each Server only supports one protocol!
The protocol will come in handy once we receive our first set of data.

## Receiving Data
Before we can receive any data at all, we must start our server:
```kotlin
server.start()
```

Next up, we need a `ServerEventListener` which notifies us if something goes wrong, or we got a new incoming connection:
```kotlin
class CustomServerEvent: ServerEventListener {
    override fun onBindFailure(e: Throwable){
        // handle errors
    }

    override fun onConnect(client: Client){
        // handle new client connection
    }
}

/*
 * register the event listener
 */
server.subscribe(CustomServerEvent())
```
Ok so now we can get notified once a new client connects. We get all information about a client through the `Client` class.    
Next up is receiving data from our client:
```kotlin
/*
 * This is the method from our class above
 */
override fun onConnect(client: Client){
    // The client also has an event handler
    client.subscribe(CustomClientEvent())
}

class CustomClientEvent: ClientEventListener {
    override fun onPacketReceive(arr: ByteArray) {
        // this method is called if no protocol was specified, we get the raw bits and bytes
    }

    override fun onPacketReceive(packet: ReadablePacket) {
        /*
         * The ReadablePacket lets us access all data we have in our protocol
         * Read more below
         */
    }

    override fun onClientDisconnected(){
        // this get called if a client disconnects or timed out
    }
}

```

## ReadablePacket and WriteablePacket
The protocol we specified earlier defines how the data structure should look like and in which order each part will arrive. The Server wraps each chunk of data that matches the protocol in a so called `Packet`. There are two types of packets: `ReadablePacket` and `WriteabePacket`. And from their name it should be obvious that one only allows reading access whereas the other allows only writing access.    

### Readabe Packet
In this `Packet`s we can access the data either by the `ProtocolField` we defined earlier or by their respective index.
> `ProtocolField("messageID", ProtocolFieldType.INT.length)` was the first we added, so it has index 0

### Writable Packet
In order to create a `WriteablePacket`, we must specifiy which the underlying protocol is:
```kotlin
// using the 'val procotol = ...' from above
var packet = WriteablePacket(protocol)

packet.setValue(0, 9283)
packet.setValue(1, "2019-01-01 00:00")
```
The `Packet` performs automatic length checks and fills empty fields with `0x00`!
We can now simply send this packet to the `Client` if we'd like:
```kotlin
client.send(packet)

//optional: forces the server to immediately send all buffered data
client.flush()
```

## Reading and Writing without Protocol and Packets
These `Protocol`s and `Packet`s make it more human friendly to work with all that stuff. But if you don't want them or use `UDP`, all `Client`s support raw data via `ByteArray`s!

## Creating Clients
To create Clients that connect to your server, simply use the `ClientFactory` object. In there are all relevant factory methods.  
Please note that a Client automatically tries to connect to a server. To start reading, please call the `start()` function.

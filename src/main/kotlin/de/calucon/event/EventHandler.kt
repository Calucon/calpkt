package de.calucon.event

/**
 * Basic [EventHandler] that allows subscription to various self-implemented events
 *
 * @param T is a interface containing all events a subscriber can listen to
 * @constructor creates a new [EventHandler]
 */
open class EventHandler<T> : IEventHandler<T> {
    protected val subscriber: MutableList<T> = mutableListOf()

    /**
     * Adds an object implementing [T] to the subscriber list
     * @param listener object to add to the list
     * @return is the listener successfully added
     */
    override fun subscribe(listener: T): Boolean = subscriber.add(listener)

    /**
     * Removes an object implementing [T] from the subscriber list
     * @param listener object to add to the list
     * @return is the listener successfully removed
     */
    override fun unsubscribe(listener: T): Boolean = subscriber.remove(listener)
}
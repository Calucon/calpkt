package de.calucon.event

/**
 * Interface that manages all event related actions
 */
interface IEventHandler<T> {

    /**
     * add a ServerEventListener object to the subscription list
     */
    fun subscribe(listener: T): Boolean

    /**
     * removes the ServerEventListener object from the subscription list
     * @return returns true if the listener was subscribed and was removed successfully
     */
    fun unsubscribe(listener: T): Boolean

}
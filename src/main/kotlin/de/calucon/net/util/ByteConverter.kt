package de.calucon.net.util

/**
 * Converts different types to [ByteArray] and back again
 */
object ByteConverter {

    fun toBytes(str: String): ByteArray = str.toByteArray()
    fun toBytes(c: Char): ByteArray = toBytes(c.code.toShort()) // char and short are 16-bit
    fun toBytes(b: Byte): ByteArray = ByteArray(1){ b }
    fun toBytes(s: Short, bigEndian: Boolean = true): ByteArray {
        val _s = s.toInt()
        val arr = byteArrayOf(
                (_s shr 8).toByte(),
                _s.toByte()
        )
        if(!bigEndian) arr.reverse() // littleEndian -> byteOrder reversed
        return arr
    }
    fun toBytes(i: Int, bigEndian: Boolean = true): ByteArray {
        val arr = byteArrayOf(
                (i shr 24).toByte(),
                (i shr 16).toByte(),
                (i shr 8).toByte(),
                i.toByte()
        )
        if(!bigEndian) arr.reverse() // littleEndian -> byteOrder reversed
        return arr
    }
    fun toBytes(l: Long, bigEndian: Boolean = true): ByteArray {
        val arr = byteArrayOf(
                (l shr 56).toByte(),
                (l shr 48).toByte(),
                (l shr 40).toByte(),
                (l shr 32).toByte(),
                (l shr 24).toByte(),
                (l shr 16).toByte(),
                (l shr 8).toByte(),
                l.toByte()
        )
        if(!bigEndian) arr.reverse() // littleEndian -> byteOrder reversed
        return arr
    }
    fun toBytes(f: Float): ByteArray = toBytes(f.toRawBits())
    fun toBytes(d: Double): ByteArray = toBytes(d.toRawBits())

    //-------------------------------------------

    fun toString(array: ByteArray): String = String(array)
    fun toChar(array: ByteArray): Char = toShort(array).toInt().toChar()
    fun toByte(array: ByteArray): Byte{
        if(array.isEmpty()) return 0
        return array[0]
    }
    fun toShort(array: ByteArray, bigEndian: Boolean = true): Short{
        var lArray = array
        if(lArray.size < 2) lArray = expandTo(lArray, 2, bigEndian)
        if(!bigEndian) lArray.reverse() // littleEndian -> byteOrder reversed

        return ((lArray[0].toInt() and 0xFF shl 8) or (lArray[1].toInt() and 0xFF)).toShort() // Short has no 'and' and 'shl' function yet!
    }
    fun toInt(array: ByteArray, bigEndian: Boolean = true): Int{
        var lArray = array
        if(lArray.size < 4) lArray = expandTo(lArray, 4, bigEndian)
        if(!bigEndian) lArray.reverse() // littleEndian -> byteOrder reversed

        return (lArray[0].toInt() and 0xFF shl 24) or (lArray[1].toInt() and 0xFF shl 16) or (lArray[2].toInt() and 0xFF shl 8) or (lArray[3].toInt() and 0xFF)
    }
    fun toLong(array: ByteArray, bigEndian: Boolean = true): Long{
        var lArray = array
        if(lArray.size < 8) lArray = expandTo(lArray, 8, bigEndian)
        if(!bigEndian) lArray.reverse() // littleEndian -> byteOrder reversed

        return (lArray[0].toLong() and 0xFFL shl 56) or (lArray[1].toLong() and 0xFFL shl 48) or(lArray[2].toLong() and 0xFFL shl 40) or(lArray[3].toLong() and 0xFFL shl 32) or
                (lArray[4].toLong() and 0xFFL shl 24) or (lArray[5].toLong() and 0xFFL shl 16) or (lArray[6].toLong() and 0xFFL shl 8) or (lArray[7].toLong() and 0xFFL)
    }
    fun toFloat(array: ByteArray): Float = Float.fromBits(toInt(array))
    fun toDouble(array: ByteArray): Double = Double.fromBits(toLong(array))

    //-------------------------------------------

    /**
     * If the [ByteArray] for the conversion back to the requested type is not long enough, we expand it while keeping the byte-order
     * @param array [ByteArray] to expand
     * @param size requested size of the [array]
     * @param bigEndian set byte-order
     * @return new [ByteArray] instance with the requested size
     */
    private fun expandTo(array: ByteArray, size: Int, bigEndian: Boolean = true): ByteArray{
        val arr = ByteArray(size)
        array.reverse() // keep the existing bytes in the correct order
        array.forEachIndexed { i, byte -> arr[if (bigEndian) (size - i - 1) else i] = byte } // bigEndian: padding bytes in front of content
        return arr
    }

}

package de.calucon.net.util

open class ThreadSleeper {

    private var sleepDuration = 0L

    //-------------------------------------------

    fun reset(){
        sleepDuration = 0L
    }
    fun increment(): Long{
        if(sleepDuration < MAX_SLEEP) sleepDuration++
        return sleepDuration
    }

    //-------------------------------------------

    internal companion object {
        private const val MAX_SLEEP = 25L
    }
}
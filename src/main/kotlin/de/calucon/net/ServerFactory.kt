package de.calucon.net

import de.calucon.net.api.InetServer
import de.calucon.net.protocol.Protocol
import de.calucon.net.tcp.TCPServer
import de.calucon.net.udp.UDPServer
import java.net.InetAddress
import java.net.InetSocketAddress

/**
 * Creates a new [InetServer]
 */
@Suppress("UNUSED", "FunctionName", "MemberVisibilityCanBePrivate")
object ServerFactory {

    /**
     * Creates a new [InetServer] based on the TCP Protocol
     * @param host address of the remote host
     * @param port port of the remove host to connect to
     */
    fun TCP(host: String, port: Int): InetServer = TCP(InetSocketAddress(host, port))
    /**
     * Creates a new [InetServer] based on the TCP Protocol
     * @param address [InetAddress] of the remote host
     * @param port port of the remove host to connect to
     */
    fun TCP(address: InetAddress, port: Int): InetServer = TCP(InetSocketAddress(address, port))
    /**
     * Creates a new [InetServer] based on the TCP Protocol
     * @param address [InetSocketAddress] of the remote host
     */
    fun TCP(address: InetSocketAddress): InetServer = TCP(address, Protocol())

    //-------------------------------------------

    /**
     * Creates a new [InetServer] based on the TCP Protocol
     * @param host address of the remote host
     * @param port port of the remove host to connect to
     */
    fun TCP(host: String, port: Int, protocol: Protocol): InetServer = TCP(InetSocketAddress(host, port), protocol)
    /**
     * Creates a new [InetServer] based on the TCP Protocol
     * @param address [InetAddress] of the remote host
     * @param port port of the remove host to connect to
     */
    fun TCP(address: InetAddress, port: Int, protocol: Protocol): InetServer = TCP(InetSocketAddress(address, port), protocol)
    /**
     * Creates a new [InetServer] based on the TCP Protocol
     * @param address [InetSocketAddress] of the remote host
     */
    fun TCP(address: InetSocketAddress, protocol: Protocol): InetServer = TCPServer(address, protocol)

    //-------------------------------------------\\
    //                  UDP                      ||
    //-------------------------------------------//

    /**
     * Creates a new [InetServer] based on the UDP Protocol
     * @param host address of the remote host
     * @param port port of the remove host to connect to
     */
    fun UDP(host: String, port: Int): InetServer = UDP(InetSocketAddress(host, port))
    /**
     * Creates a new [InetServer] based on the UDP Protocol
     * @param address [InetAddress] of the remote host
     * @param port port of the remove host to connect to
     */
    fun UDP(address: InetAddress, port: Int): InetServer = UDP(InetSocketAddress(address, port))
    /**
     * Creates a new [InetServer] based on the UDP Protocol
     * @param address [InetSocketAddress] of the remote host
     */
    fun UDP(address: InetSocketAddress): InetServer = UDP(address, Protocol())

    //-------------------------------------------

    /**
     * Creates a new [InetServer] based on the UDP Protocol
     * @param host address of the remote host
     * @param port port of the remove host to connect to
     */
    fun UDP(host: String, port: Int, protocol: Protocol): InetServer = UDP(InetSocketAddress(host, port), protocol)
    /**
     * Creates a new [InetServer] based on the UDP Protocol
     * @param address [InetAddress] of the remote host
     * @param port port of the remove host to connect to
     */
    fun UDP(address: InetAddress, port: Int, protocol: Protocol): InetServer = TCP(InetSocketAddress(address, port), protocol)
    /**
     * Creates a new [InetServer] based on the UDP Protocol
     * @param address [InetSocketAddress] of the remote host
     */
    fun UDP(address: InetSocketAddress, protocol: Protocol): InetServer = UDPServer(address, protocol)

}
package de.calucon.net.tcp

import de.calucon.net.InetServerBase
import de.calucon.net.api.InetServer
import de.calucon.net.api.ServerType
import de.calucon.net.client.TCPClient
import de.calucon.net.protocol.Protocol
import java.io.FileInputStream
import java.net.InetSocketAddress
import java.security.KeyStore
import java.security.SecureRandom
import javax.net.ssl.*

class TCPSSLServer constructor(address: InetSocketAddress, protocol: Protocol) : InetServerBase(address, ServerType.TCP_SSL, protocol), InetServer {

    private val socket: SSLServerSocket

    init {
        val ssf: SSLServerSocketFactory = SSLServerSocketFactory.getDefault() as SSLServerSocketFactory

        socket = ssf.createServerSocket() as SSLServerSocket
    }

    //-------------------------------------------

    private val serverThread = Thread {
        // bind server to address
        try {
            socket.bind(address, DEFAULT_BACKLOG)
        } catch (exception: Throwable){
            subscriber.forEach { it.onBindFailure(exception) }
        }

        while(!Thread.interrupted() && socket.isBound){
            val cSock: SSLSocket = socket.accept() as SSLSocket
            cSock.startHandshake()

            val client = TCPClient(cSock, protocol)
            subscriber.forEach { it.onConnect(client) }
            client.start()
        }
    }

    //-------------------------------------------

    fun init0(keystoreFilePath: String, keystorePassword: String){
        System.setProperty("javax.net.ssl.keyStore", keystoreFilePath)
        System.setProperty("javax.net.ssl.keyStorePassword", keystorePassword)
    }

    fun init1(keystoreFilePath: String, keystorePassword: String) {
        val ks = KeyStore.getInstance("JKS") //jsk only in java -> android uses bks, ...
        ks.load(FileInputStream(keystoreFilePath), keystorePassword.toCharArray())

        val kmf = KeyManagerFactory.getInstance("X509")
        kmf.init(ks, keystorePassword.toCharArray())

        val tmf = TrustManagerFactory.getInstance("TLS")
        tmf.init(ks)

        val context: SSLContext = SSLContext.getInstance("TLS")
        context.init(kmf.keyManagers, tmf.trustManagers, SecureRandom())
    }

    //-------------------------------------------

    /**
     * start server thread
     */
    override fun start() {
        serverThread.start()
    }

    /**
     * stop server thread
     */
    override fun stop() {
        if(!serverThread.isInterrupted) serverThread.interrupt()
    }
}
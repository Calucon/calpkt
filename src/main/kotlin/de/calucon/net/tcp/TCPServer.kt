package de.calucon.net.tcp

import de.calucon.net.InetServerBase
import de.calucon.net.api.InetServer
import de.calucon.net.api.ServerType
import de.calucon.net.client.TCPClient
import de.calucon.net.protocol.Protocol
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.net.SocketException

class TCPServer internal constructor(address: InetSocketAddress, protocol: Protocol) : InetServerBase(address, ServerType.TCP, protocol), InetServer {

    private val socket: ServerSocket = ServerSocket()

    //-------------------------------------------

    private val serverThread = Thread {
        // bind server to address
        try {
            socket.bind(address, DEFAULT_BACKLOG)
        } catch (exception: Throwable){
            subscriber.forEach { it.onBindFailure(exception) }
        }

        while(!Thread.interrupted() && socket.isBound){
            val client: TCPClient? = try { TCPClient(socket.accept(), protocol) } catch (e: SocketException){ null }

            if(client != null){
                subscriber.forEach { it.onConnect(client) }
            }
        }
    }

    //-------------------------------------------

    /**
     * start server thread
     */
    override fun start() {
        serverThread.start()
    }

    /**
     * stop server thread
     */
    override fun stop() {
        if(!serverThread.isInterrupted) {
            serverThread.interrupt()
            socket.close()
        }
    }
}
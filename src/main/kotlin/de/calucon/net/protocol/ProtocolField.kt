package de.calucon.net.protocol

import de.calucon.net.api.util.Serializable
import de.calucon.net.protocol.exception.ProtocolFieldInvalidValueException
import de.calucon.net.protocol.exception.ProtocolFieldLengthException
import de.calucon.net.util.ByteConverter
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream

/**
 * @property name Name of the Field
 * @property length length in bytes of the values
 * @property accepts Array of types that are allowed when creating such a field for a packet
 * @constructor Creates a ProtocolField that can be used in a [Protocol].
 */
@Suppress("MemberVisibilityCanBePrivate")
class ProtocolField constructor(val name: String, val length: Int, val accepts: Array<Any>? = null) {
    //constructor(name: String, fieldType: ProtocolFieldType, accepts: Array<Any>? = null): this(name, fieldType.length, accepts)

    companion object {
        const val PADDING_BYTE = 0.toByte()
    }

    //-------------------------------------------

    /**
     * gets calculated when the protocol get's created.
     * It facilitates inserting bytes to the final packet
     */
    var packetIndex: Int = 0

    fun toBytes(obj: Any): ByteArray{
        return when(obj){
            is String -> toBytes(obj)
            is Char -> toBytes(obj)
            is Byte -> toBytes(obj)
            is Short -> toBytes(obj)
            is Int -> toBytes(obj)
            is Long -> toBytes(obj)
            is Float -> toBytes(obj)
            is Double -> toBytes(obj)
            is Serializable -> toBytes(obj)
            is java.io.Serializable -> toBytes(obj) // must be at the end ... seems like Short implements it
            else -> throw ProtocolFieldInvalidValueException("Your Value must be a primitive type, String or implement either 'de.calucon.net.api.util.Serializable' or 'java.io.Serializable'")
        }
    }
    private fun toBytes(obj: java.io.Serializable): ByteArray {
        val out = ByteArrayOutputStream()

        // this converts the object to a ByteArray and stores it in [out]
        ObjectOutputStream(out).use { it.writeObject(obj) }

        return toBytes(out.toByteArray())
    }
    private fun toBytes(obj: Serializable) : ByteArray = toBytes(obj.serialize())
    private fun toBytes(str: String): ByteArray = toBytes(ByteConverter.toBytes(str))
    private fun toBytes(c: Char): ByteArray = toBytes(ByteConverter.toBytes(c))
    private fun toBytes(b: Byte): ByteArray = toBytes(ByteConverter.toBytes(b))
    private fun toBytes(s: Short): ByteArray = toBytes(ByteConverter.toBytes(s))
    private fun toBytes(i: Int): ByteArray = toBytes(ByteConverter.toBytes(i))
    private fun toBytes(l: Long): ByteArray = toBytes(ByteConverter.toBytes(l))
    private fun toBytes(f: Float): ByteArray = toBytes(ByteConverter.toBytes(f))
    private fun toBytes(d: Double): ByteArray = toBytes(ByteConverter.toBytes(d))

    /**
     * this method will always get called and does the remaining integrity checks
     */
    private fun toBytes(arr: ByteArray): ByteArray {
        lengthCheck(arr)
        return arr
    }

    //-------------------------------------------

    /**
     * Creates a new [ByteArray] with the length of the field.
     * First all existing Bytes are inserted and the rest is filled up with [PADDING_BYTE]
     * @param arr [ByteArray] to expand
     */
    private fun addPaddingBytes(arr: ByteArray){
        if(arr.size < length){
            val expandedArray = ByteArray(length)
            for(i in 0 until length)
                expandedArray[i] = arr.getOrElse(i){ PADDING_BYTE }
        }
    }

    /**
     * Checks if the [arr] matches the length of the protocol field
     * If not, it get's expanded.
     * If it's to long, an exception get's thrown
     * @param arr [ByteArray] to check
     * @throws ProtocolFieldLengthException if the serialized object is too long for the field
     */
    private fun lengthCheck(arr: ByteArray){
        if(arr.size > length) throw ProtocolFieldLengthException("The object has ${arr.size} Bytes. The maximum allowed length is $length Bytes for this field.")
        else addPaddingBytes(arr)
    }

    fun isValueAccepted(obj: Any): Boolean{
        return accepts?.contains(obj) ?: true // check if this is valid -> e.g. String implementation in Java
    }
}
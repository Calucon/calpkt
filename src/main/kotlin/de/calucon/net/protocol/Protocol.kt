package de.calucon.net.protocol

/**
 * Protocol Length is determined by the total length of all protocol fields
 */
class Protocol constructor(vararg protocolFields: ProtocolField) {

    val totalFieldLength: Int
    val protocolFields: Array<ProtocolField>
    init {
        var totalFieldLength = 0
        this.protocolFields = emptyArray<ProtocolField>().plus(protocolFields)

        this.protocolFields.forEach {
            it.packetIndex = totalFieldLength
            totalFieldLength += it.length
        }
        this.totalFieldLength = totalFieldLength
    }

    //-------------------------------------------

    fun getReadablePacket(): ReadablePacket = ReadablePacket(this)
    fun getWriteablePacket(): WriteablePacket = WriteablePacket(this)
}
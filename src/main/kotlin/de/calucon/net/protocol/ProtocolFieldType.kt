package de.calucon.net.protocol

enum class ProtocolFieldType(val length: Int) {

    BYTE(1),
    SHORT(2),
    INT(4),
    LONG(8),

    FLOAT(4),
    DOUBLE(8)

}
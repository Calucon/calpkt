package de.calucon.net.protocol.exception

class ProtocolFieldInvalidValueException(override var message: String): Exception(message)
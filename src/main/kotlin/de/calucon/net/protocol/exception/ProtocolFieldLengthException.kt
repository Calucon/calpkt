package de.calucon.net.protocol.exception

class ProtocolFieldLengthException(override var message: String): Exception(message)
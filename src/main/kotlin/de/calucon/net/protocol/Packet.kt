package de.calucon.net.protocol

open class Packet protected constructor(protected val protocol: Protocol) {

    protected val data: ByteArray = ByteArray(protocol.totalFieldLength)

    fun getBytes(): ByteArray = data.clone()
}


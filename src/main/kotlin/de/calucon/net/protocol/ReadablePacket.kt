package de.calucon.net.protocol

import de.calucon.net.protocol.exception.ProtocolFieldInvalidValueException
import de.calucon.net.util.ByteConverter
import java.util.*

class ReadablePacket(protocol: Protocol) : Packet(protocol) {

    fun getArray(fieldIndex: Int): ByteArray{
        val field: ProtocolField? = protocol.protocolFields.getOrNull(fieldIndex)
        if(field != null){
            return getArray(field)
        } else throw ProtocolFieldInvalidValueException("The field you specified is null")
    }
    fun getArray(field: ProtocolField): ByteArray{
        if(protocol.protocolFields.contains(field)){
            return data.copyOfRange(field.packetIndex, (field.packetIndex + field.length))
        } else throw ProtocolFieldInvalidValueException("The field you specified is not part of the protocol!")
    }

    fun getString(fieldIndex: Int, trimNullChar: Boolean = true): String = getString(getArray(fieldIndex), trimNullChar)
    fun getString(field: ProtocolField, trimNullChar: Boolean = true): String = getString(getArray(field), trimNullChar)
    private fun getString(data: ByteArray, trimNullChar: Boolean): String {
        val str = ByteConverter.toString(data)
        return if(trimNullChar) str.trim('\u0000') else str
    }

    fun getChar(fieldIndex: Int): Char = ByteConverter.toChar(getArray(fieldIndex))
    fun getChar(field: ProtocolField): Char = ByteConverter.toChar(getArray(field))

    fun getByte(fieldIndex: Int): Byte = ByteConverter.toByte(getArray(fieldIndex))
    fun getByte(field: ProtocolField): Byte = ByteConverter.toByte(getArray(field))

    fun getShort(fieldIndex: Int): Short = ByteConverter.toShort(getArray(fieldIndex))
    fun getShort(field: ProtocolField): Short = ByteConverter.toShort(getArray(field))

    fun getInt(fieldIndex: Int): Int = ByteConverter.toInt(getArray(fieldIndex))
    fun getInt(field: ProtocolField): Int = ByteConverter.toInt(getArray(field))

    fun getLong(fieldIndex: Int): Long = ByteConverter.toLong(getArray(fieldIndex))
    fun getLong(field: ProtocolField): Long = ByteConverter.toLong(getArray(field))

    fun getFloat(fieldIndex: Int): Float = ByteConverter.toFloat(getArray(fieldIndex))
    fun getFloat(field: ProtocolField): Float = ByteConverter.toFloat(getArray(field))

    fun getDouble(fieldIndex: Int): Double = ByteConverter.toDouble(getArray(fieldIndex))
    fun getDouble(field: ProtocolField): Double = ByteConverter.toDouble(getArray(field))

    //-------------------------------------------

    private var totalBytesRead = 0

    /**
     * Appends the read bytes to this packet.
     * @param bytes [ByteArray] received from our input stream
     * @param bytesRead [Int] indicating how much bytes are there in the buffer
     * @return [ByteArray] if there is data left over, otherwise it will return null
     */
    internal fun read(bytes: ByteArray, bytesRead: Int): ByteArray? {
        if(bytesRead == 0) return null
        val spaceLeft = protocol.totalFieldLength - totalBytesRead

        // all fit in...append all
        return if(spaceLeft > bytesRead){
            for(i in 0 until bytesRead) data[totalBytesRead++] = bytes[i]

            null
        } else { // example: size = 64, spaceLeft = 64 and now the result is 0 -> infinity loop
            for(i in 0 until spaceLeft) data[totalBytesRead++] = bytes[i]

            if (bytesRead == spaceLeft) ByteArray(0) else bytes.copyOfRange(spaceLeft, bytesRead)
        }
    }
}
package de.calucon.net.protocol

import de.calucon.net.protocol.exception.ProtocolFieldInvalidValueException

class WriteablePacket(protocol: Protocol) : Packet(protocol) {
    
    fun setValue(fieldIndex: Int, obj: Any){
        val field: ProtocolField? = protocol.protocolFields.getOrNull(fieldIndex)
        if(field != null){
            setValue(field, obj)
        } else throw ProtocolFieldInvalidValueException("The field you specified is null")
    }

    fun setValue(field: ProtocolField, obj: Any){
        if(protocol.protocolFields.contains(field)){
            if(field.isValueAccepted(obj)){
                val arr = field.toBytes(obj)
                for(i in arr.indices) data[field.packetIndex + i] = arr[i] // fill packet byte array
            } else throw ProtocolFieldInvalidValueException("the value you entered is not accepted by this field!")
        } else throw ProtocolFieldInvalidValueException("The field you specified is not part of the protocol!")
    }
}
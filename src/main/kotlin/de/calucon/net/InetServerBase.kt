package de.calucon.net

import de.calucon.event.EventHandler
import de.calucon.net.api.listener.ServerEventListener
import de.calucon.net.api.InetServerBase
import de.calucon.net.api.ServerType
import de.calucon.net.protocol.Protocol
import java.net.InetAddress
import java.net.InetSocketAddress

open class InetServerBase protected constructor(private val address: InetSocketAddress, private val serverType: ServerType, val protocol: Protocol) : EventHandler<ServerEventListener>(), InetServerBase {
    override fun getAddress(): InetAddress = address.address
    override fun getPort(): Int = address.port
    override fun getServerType(): ServerType = serverType

    protected companion object {
        const val DEFAULT_BACKLOG = 128
    }
}
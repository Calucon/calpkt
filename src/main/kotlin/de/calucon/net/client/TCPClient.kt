package de.calucon.net.client

import de.calucon.event.EventHandler
import de.calucon.net.api.listener.ClientEventListener
import de.calucon.net.protocol.Packet
import de.calucon.net.protocol.Protocol
import de.calucon.net.protocol.ReadablePacket
import de.calucon.net.util.ThreadSleeper
import java.io.BufferedInputStream
import java.net.InetAddress
import java.net.Socket
import java.net.SocketException

/**
 * represents a Socket that is connected on a TCPServer
 */
class TCPClient internal constructor(private val socket: Socket, val protocol: Protocol): EventHandler<ClientEventListener>(), de.calucon.net.api.client.Client {

    private val sleeper = ThreadSleeper()
    private val hasProtocol: Boolean = protocol.totalFieldLength > 0

    private val out = socket.getOutputStream()
    private val inputStream = BufferedInputStream(socket.getInputStream())

    private val clientThread = Thread({
        val arr = ByteArray(getPacketLength())
        var read: Int
        var currentPacket: ReadablePacket = protocol.getReadablePacket()
        var leftOverArr: ByteArray?

        while(!Thread.interrupted()){
            // only execute if there are any subscribers to receive the data
            if(subscriber.size > 0 && !socket.isInputShutdown){
                // try read and on exception close
                read = try {
                    inputStream.read(arr)
                } catch (e: Exception) {
                    when(e) {
                        is SocketException,
                        is InterruptedException -> {
                            close()
                            0
                        }
                        else -> throw e
                    }
                }

                // check if at least one byte was read
                when {
                    read > 0 -> {
                        sleeper.reset()
                        if(hasProtocol){
                            leftOverArr = currentPacket.read(arr, read)
                            while(leftOverArr != null){ // while loop required here (packet.size = 10 -> read = 30 => result: 3x packet
                                subscriber.forEach { it.onPacketReceive(currentPacket) }
                                currentPacket = protocol.getReadablePacket()
                                leftOverArr = if(leftOverArr.isNotEmpty()) currentPacket.read(leftOverArr, leftOverArr.size) else null // escape loop if [leftOverArr] is empty as there is no data left
                            }
                        } else {
                            subscriber.forEach { it.onPacketReceive(arr.copyOfRange(0, read)) }
                        }
                    }
                    read == -1 -> onDisconnect()
                    else -> try { Thread.sleep(sleeper.increment()) } catch (e: InterruptedException) { /*nothing*/ }
                }
            } else try { Thread.sleep(sleeper.increment()) } catch (e: InterruptedException) { /*nothing*/ }
            if(!socket.isConnected || socket.isClosed) onDisconnect()
        }
    }, "${javaClass.simpleName}@${socket.inetAddress}:${socket.port}")

    //-------------------------------------------

    /**
     * Starts reading from the remote Host in a separate thread
     * @return returns the Thread the client runs in
     */
    override fun start() {
        clientThread.start()
    }

    /**
     * Send a message to the Host this [TCPClient] is connected to
     * @param arr ByteArray containing the data to send
     */
    override fun write(arr: ByteArray) = out.write(arr)
    /**
     * Send a message to the Host this [TCPClient] is connected to
     * @param packet [Packet] containing the data to send
     */
    override fun write(packet: Packet) = write(packet.getBytes())
    /**
     * flushes the [out] OutputStream
     */
    override fun flush() = out.flush()
    /**
     * Closes the [Socket] connection to the remote host
     */
    override fun close() {
        flush()
        out.close()
        inputStream.close()
        socket.close()
        clientThread.interrupt()
    }
    /**
     * @return the [InetAddress] of this [Socket]
     */
    override fun getInetAddress(): InetAddress = socket.inetAddress

    //-------------------------------------------

    private fun onDisconnect() {
        close()
        subscriber.forEach { it.onClientDisconnected() }
    }

    private fun getPacketLength(): Int{
        return if(protocol.totalFieldLength < BUFFER_DEFAULT) BUFFER_DEFAULT else protocol.totalFieldLength
    }

    //-------------------------------------------

    companion object {
        const val BUFFER_DEFAULT = 2048

        fun from(host: String, port: Int, protocol: Protocol): TCPClient {
            val socket = Socket(host, port)
            return TCPClient(socket, protocol)
        }

        fun from(address: InetAddress, port: Int, protocol: Protocol): TCPClient {
            val socket = Socket(address, port)
            return TCPClient(socket, protocol)
        }
    }
}
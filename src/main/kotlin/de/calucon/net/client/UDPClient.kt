package de.calucon.net.client

import de.calucon.event.EventHandler
import de.calucon.net.api.client.Client
import de.calucon.net.api.listener.ClientEventListener
import de.calucon.net.protocol.Packet
import de.calucon.net.protocol.Protocol
import de.calucon.net.protocol.ReadablePacket
import de.calucon.net.udp.UDPServer
import de.calucon.net.util.ThreadSleeper
import java.net.DatagramPacket
import java.net.InetAddress
import java.util.*

class UDPClient(private val server: UDPServer, private val address: InetAddress, val protocol: Protocol) : EventHandler<ClientEventListener>(), Client {

    private val hasProtocol: Boolean = protocol.totalFieldLength > 0
    private val queue: LinkedList<DatagramPacket> = LinkedList()
    private val sleeper = ThreadSleeper()

    //-------------------------------------------

    @Synchronized
    internal fun packetReceived(packet: DatagramPacket) = queue.add(packet)

    private val clientThread = Thread({
        var currentPacket: ReadablePacket = protocol.getReadablePacket()
        var leftOverArr: ByteArray?

        while(!Thread.interrupted()) {
            if(subscriber.size > 0){
                synchronized(this){
                    if(queue.isNotEmpty()){
                        val packet: DatagramPacket = queue.pollFirst()

                        if(hasProtocol){
                            leftOverArr = currentPacket.read(packet.data, packet.length)
                            while(leftOverArr != null){ // while loop required here (packet.size = 10 -> read = 30 => result: 3x packet
                                subscriber.forEach { it.onPacketReceive(currentPacket) }
                                currentPacket = protocol.getReadablePacket()
                                leftOverArr = currentPacket.read(leftOverArr!!, leftOverArr!!.size)
                            }
                        } else {
                            subscriber.forEach { it.onPacketReceive(packet.data) }
                        }
                        sleeper.reset()
                    } else try { Thread.sleep(sleeper.increment()) } catch (e: InterruptedException) { /*nothing*/ }
                }
            } else try { Thread.sleep(sleeper.increment()) } catch (e: InterruptedException) { /*nothing*/ }
        }
    }, "${javaClass.simpleName}@${address}:${server.getPort()}")

    override fun start() {
        clientThread.start()
    }

    //-------------------------------------------

    override fun write(arr: ByteArray) = server.sendPacket(arr, address)
    override fun write(packet: Packet) = write(packet.getBytes())
    override fun flush() { /* this has no function in udp */ }
    override fun close() {
        flush()
        server.disconnectClient(this)
        subscriber.forEach { it.onClientDisconnected() }
        clientThread.interrupt()
    }

    override fun getInetAddress(): InetAddress = address
}
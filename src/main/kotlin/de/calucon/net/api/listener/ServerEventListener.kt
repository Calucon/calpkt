package de.calucon.net.api.listener

import de.calucon.net.api.client.Client

/**
 * Interface containing all events that can be raised by each InetServer
 */
interface ServerEventListener {

    /**
     * called when an error occurs during bind
     */
    fun onBindFailure(e: Throwable)

    /**
     * called when a new client is accepted
     */
    fun onConnect(client: Client)

}
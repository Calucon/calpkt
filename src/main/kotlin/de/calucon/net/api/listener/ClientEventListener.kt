package de.calucon.net.api.listener

import de.calucon.net.protocol.ReadablePacket

/**
 * Interface containing all events that can be raised by an accepted TCPClient of the InetServer
 */
interface ClientEventListener {

    /**
     * called when the server receives any data and not protocol is specified
     */
    fun onPacketReceive(arr: ByteArray)

    /**
     * called when the server received a full packet
     */
    fun onPacketReceive(packet: ReadablePacket)

    /**
     * called when a client looses connection or disconnects on it's own
     */
    fun onClientDisconnected()
}
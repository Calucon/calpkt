package de.calucon.net.api.client

import de.calucon.event.IEventHandler
import de.calucon.net.api.listener.ClientEventListener
import de.calucon.net.protocol.Packet
import java.net.InetAddress

/**
 * represents a Socket that is connected on a TCPServer
 */
interface Client : IEventHandler<ClientEventListener> {

    /**
     * Starts reading from the remote Host in a separate thread
     */
    fun start()

    /**
     * writes to output stream
     */
    fun write(arr: ByteArray)

    /**
     * writes to output stream
     */
    fun write(packet: Packet)

    /**
     * disconnects the client
     */
    fun close()

    /**
     * get the remote address of the client
     */
    fun getInetAddress(): InetAddress

    /**
     * Flushes the OutputStream
     */
    fun flush()
}
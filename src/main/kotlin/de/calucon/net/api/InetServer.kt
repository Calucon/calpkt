package de.calucon.net.api

/**
 * Interface for all functions whose implementation differs on each instance type
 */
interface InetServer : InetServerBase {

    /**
     * starts the server
     */
    fun start()

    /**
     * stops the server
     */
    fun stop()

}
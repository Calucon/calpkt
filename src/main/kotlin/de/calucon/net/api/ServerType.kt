package de.calucon.net.api

enum class ServerType {
    TCP,
    TCP_SSL,

    UDP
}
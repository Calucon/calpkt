package de.calucon.net.api.ssl

enum class KeyStoreProvider {

    JKS,            // default
    BKS,            // Android default
    PKCS11,
    PKCS12,
    Windows_MY,     // Windows cert store
    Windows_ROOT,   // Windows cert store
    KeychainStore   // OSX cert store

    //https://stackoverflow.com/a/11540061/6597672

}
package de.calucon.net.api

import de.calucon.event.IEventHandler
import de.calucon.net.api.listener.ServerEventListener
import java.net.InetAddress

/**
 * Interface for all functions whose implementation is equal on all instance types
 */
interface InetServerBase : IEventHandler<ServerEventListener> {

    /**
     * @return InetAddress of the server instance
     */
    fun getAddress(): InetAddress

    /**
     * @return Port of the server instance
     */
    fun getPort(): Int

    /**
     * @return Protocol type of this InetServer
     */
    fun getServerType(): ServerType

}
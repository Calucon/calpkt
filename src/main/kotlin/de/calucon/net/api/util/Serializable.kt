package de.calucon.net.api.util

/**
 * java's Serializable interface is not the best so we want devs to be able to provide their own algorithms
 */
interface Serializable {

    /**
     * Convert object to a [ByteArray]
     */
    fun serialize(): ByteArray

    /**
     * Converts [arr] to a new instance of the object
     */
    fun deserialize(arr: ByteArray)

}
package de.calucon.net

import de.calucon.net.api.client.Client
import de.calucon.net.client.TCPClient
import de.calucon.net.protocol.Protocol
import java.net.InetAddress

/**
 * Creates a new [Client]
 */
@Suppress("FunctionName", "MemberVisibilityCanBePrivate", "unused")
object ClientFactory {

    /**
     * Creates a new [Client] based on the TCP Protocol
     * @param host address of the remote host
     * @param port port of the remove host to connect to
     */
    fun TCP(host: String, port: Int): Client = TCP(host, port, Protocol())

    /**
     * Creates a new [Client] based on the TCP Protocol
     * @param address address of the remote host
     * @param port port of the remove host to connect to
     */
    fun TCP(address: InetAddress, port: Int): Client = TCP(address, port, Protocol())

    //-------------------------------------------

    /**
     * Creates a new [Client] based on the TCP Protocol
     * @param host address of the remote host
     * @param port port of the remove host to connect to
     */
    fun TCP(host: String, port: Int, protocol: Protocol): Client = TCPClient.from(host, port, protocol)

    /**
     * Creates a new [Client] based on the TCP Protocol
     * @param address address of the remote host
     * @param port port of the remove host to connect to
     */
    fun TCP(address: InetAddress, port: Int, protocol: Protocol): Client = TCPClient.from(address, port, protocol)

}
package de.calucon.net.udp

import de.calucon.net.InetServerBase
import de.calucon.net.api.InetServer
import de.calucon.net.api.ServerType
import de.calucon.net.api.client.Client
import de.calucon.net.client.TCPClient
import de.calucon.net.client.UDPClient
import de.calucon.net.protocol.Protocol
import de.calucon.net.util.ThreadSleeper
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.InetSocketAddress

class UDPServer internal constructor(address: InetSocketAddress, protocol: Protocol): InetServerBase(address, ServerType.UDP, protocol), InetServer {

    // bind server to address
    private val socket: DatagramSocket = try {
        DatagramSocket(address)
    } catch (exception: Throwable){
        subscriber.forEach { it.onBindFailure(exception) }
        DatagramSocket()
    }

    private val clientMapping: HashMap<InetAddress, UDPClient> = hashMapOf()
    private val sleeper = ThreadSleeper()
    private val arr = ByteArray(if(protocol.totalFieldLength > 0) protocol.totalFieldLength else TCPClient.BUFFER_DEFAULT)
    private val arrSize = arr.size

    //-------------------------------------------

    private val serverThread = Thread {

        while(!Thread.interrupted() && socket.isBound){
            val packet = DatagramPacket(arr, arrSize)
            socket.receive(packet)

            // check if any data was read
            if(packet.length > 0){
                // init new client if there is none
                var client: UDPClient? = clientMapping[packet.address]
                if(client == null){
                    client = UDPClient(this, packet.address, protocol)
                    clientMapping[packet.address] = client
                    subscriber.forEach { it.onConnect(client) }
                    client.start()
                }

                // notify client
                client.packetReceived(packet)
                sleeper.reset()
            } else Thread.sleep(sleeper.increment())
        }
    }

    //-------------------------------------------

    /**
     * Removes the Client from our internal mapping
     */
    internal fun disconnectClient(client: Client) = clientMapping.remove(client.getInetAddress())

    /**
     * sends a [DatagramPacket] back to the remote host
     */
    internal fun sendPacket(data: ByteArray, receiver: InetAddress){
        val packet = DatagramPacket(data, data.size, receiver, socket.port)
        socket.send(packet)
    }

    //-------------------------------------------

    /**
     * start server thread
     */
    override fun start() {
        serverThread.start()
    }

    /**
     * stop server thread
     */
    override fun stop() {
        if(!serverThread.isInterrupted) serverThread.interrupt()
    }

}